from mmdet.apis import init_detector, inference_detector
import mmcv
import glob
import time
from pycocotools import mask
import numpy as np


class Yolact:
    def __init__(self):
        self.det_treshold = 0.7

        # Specify the path to model config and checkpoint file
        self.config_file = 'configs/yolact_r101_1x8_coco.py'
        self.checkpoint_file = 'ml_vision_input_folder/yolact_r101_1x8_coco/epoch_25.pth'

        # build the model from a config file and a checkpoint file
        self.model = init_detector(self.config_file, self.checkpoint_file, device='cuda:0')
        self.classmap = ['banana', 'blade', 'bowl', 'broccoli', 'broccoli_handle', 'broccoli_head', 'carrot',
                         'cucumber',
                         'cutting_board', 'handle', 'knife', 'lipton', 'milk', 'onion', 'onion_chives', 'onion_root',
                         'orange',
                         'plate', 'spatula', 'spatula_tool']

        # self.img_list = glob.glob('/home/lewiatan/DATASETS/coppelia_dataset/val/*.png')
        # times_list = list()

    def detect_image(self, img):
        start = time.time()
        result = inference_detector(self.model, img)
        result_dict = {
            "classes": list(),
            "masks": list(),
            "scores": list(),
            "class_ids": list()
        }
        classes_list = [cls for cls in result[0]]
        masks_list = [msk for msk in result[1]]
        c = 0
        for tpl in zip(classes_list, masks_list):
            m = 0
            for el in tpl[0]:
                if self.det_treshold < el[4]:
                    # appending class names if the detection confidence gt threshold
                    result_dict["classes"].append(self.classmap[c])
                    # appending confidence score values which are at index 4 of the instances np array
                    result_dict["scores"].append(el[4])
                    # appending numeric class ids
                    result_dict["class_ids"].append(c + 1)
                    # get mask of the current instance
                    msk = tpl[1][m]
                    # encode the mask with rle and append to results dict
                    res = mask.encode(np.asfortranarray(msk))
                    res["counts"] = res["counts"].decode()
                    result_dict["masks"].append(res)
                m += 1
            c += 1
        return result_dict

